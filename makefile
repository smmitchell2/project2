OBJS = dll.o vertex.o edge.o graph.o prim.o
OPTS = -Wall -g -std=c99

prim : $(OBJS)
	gcc $(OPTS) $(OBJS) -o prim -lm

prim.o : prim.c graph.h edge.h vertex.h dll.h
	gcc $(OPTS) -c prim.c

dll.o : dll.c dll.h
	gcc $(OPTS) -c dll.c

graph.o : graph.c graph.h vertex.h edge.h dll.h
	gcc $(OPTS) -c graph.c

edge.o : edge.c edge.h vertex.h dll.h
	gcc $(OPTS) -c edge.c

vertex.o : vertex.c vertex.h edge.h dll.h
	gcc $(OPTS) -c vertex.c

dll.o : dll.c dll.h
	gcc $(OPTS) -c dll.c

clean :
	rm -f $(OBJS) prim

test :
	@echo TESTING BST TREE
	@echo trees -b s.txt command.txt
	@echo ###############################
	trees -b s.txt command.txt
	@echo ###############################
	@echo TESTING AVL TREE
	@echo trees -a s.txt command.txt
	@echo ###############################
	trees -a s.txt command.txt
	@echo ###############################
